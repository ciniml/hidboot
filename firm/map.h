#ifndef __MAP_H__
/**
 * @file map.h
 * 
 * このファイルはgpiomap.pyにより自動的に生成されました．
 * ポートの割り当ての設定を行います．
 */

#define	MAP_CC(a, b)	a ## b
#define	MAP_OUT(name)	MAP_CC(PORT, name)
#define	MAP_IN(name)	MAP_CC(PIN, name)
#define	MAP_DIR(name)	MAP_CC(DDR, name)

//definition for BOOTMODE
#define	PORTNAME_BOOTMODE	D
#define	BIT_BOOTMODE	4
#define	PORT_BOOTMODE	MAP_OUT(PORTNAME_BOOTMODE)
#define	PIN_BOOTMODE	MAP_IN(PORTNAME_BOOTMODE)
#define	DDR_BOOTMODE	MAP_DIR(PORTNAME_BOOTMODE)

//definition for BOOTMODE
#define	PORTNAME_TESTLED	D
#define	BIT_TESTLED	0
#define	PORT_TESTLED	MAP_OUT(PORTNAME_TESTLED)
#define	PIN_TESTLED	MAP_IN(PORTNAME_TESTLED)
#define	DDR_TESTLED	MAP_DIR(PORTNAME_TESTLED)

#define BOOTMODE_PULLUP		1
#define CHECK_BOOTMODE()	(PIN_BOOTMODE & _BV(BIT_BOOTMODE))

#endif	//__MAP_H__
