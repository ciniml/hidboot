﻿namespace hidbootwin
{
    partial class MainForm
    {
        /// <summary>
        /// 必要なデザイナ変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージ リソースが破棄される場合 true、破棄されない場合は false です。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナで生成されたコード

        /// <summary>
        /// デザイナ サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディタで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.runButton = new System.Windows.Forms.Button();
            this.binaryText = new System.Windows.Forms.TextBox();
            this.browseButton = new System.Windows.Forms.Button();
            this.openTarget = new System.Windows.Forms.OpenFileDialog();
            this.SuspendLayout();
            // 
            // runButton
            // 
            this.runButton.Location = new System.Drawing.Point(294, 12);
            this.runButton.Name = "runButton";
            this.runButton.Size = new System.Drawing.Size(66, 22);
            this.runButton.TabIndex = 0;
            this.runButton.Text = "実行 (&R)";
            this.runButton.UseVisualStyleBackColor = true;
            this.runButton.Click += new System.EventHandler(this.connectButton_Click);
            // 
            // binaryText
            // 
            this.binaryText.Location = new System.Drawing.Point(12, 14);
            this.binaryText.Name = "binaryText";
            this.binaryText.ReadOnly = true;
            this.binaryText.Size = new System.Drawing.Size(204, 19);
            this.binaryText.TabIndex = 1;
            // 
            // browseButton
            // 
            this.browseButton.Location = new System.Drawing.Point(222, 12);
            this.browseButton.Name = "browseButton";
            this.browseButton.Size = new System.Drawing.Size(66, 22);
            this.browseButton.TabIndex = 2;
            this.browseButton.Text = "参照 (&B)";
            this.browseButton.UseVisualStyleBackColor = true;
            this.browseButton.Click += new System.EventHandler(this.browseButton_Click);
            // 
            // openTarget
            // 
            this.openTarget.DefaultExt = "hex";
            this.openTarget.FileName = "*.hex";
            this.openTarget.Filter = "Hex ファイル|*.hex|すべてのファイル|*.*";
            this.openTarget.RestoreDirectory = true;
            this.openTarget.SupportMultiDottedExtensions = true;
            this.openTarget.Title = "実行するファイルを指定";
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 43);
            this.Controls.Add(this.browseButton);
            this.Controls.Add(this.binaryText);
            this.Controls.Add(this.runButton);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.MaximizeBox = false;
            this.Name = "MainForm";
            this.Text = "HIDBoot";
            this.TopMost = true;
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button runButton;
        private System.Windows.Forms.TextBox binaryText;
        private System.Windows.Forms.Button browseButton;
        private System.Windows.Forms.OpenFileDialog openTarget;
    }
}

