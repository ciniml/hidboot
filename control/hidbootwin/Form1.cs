using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.IO;
using HidBootLib;
using AvrLib.Image;

namespace hidbootwin
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (this.binaryText.Text.Length == 0)
            {
                MessageBox.Show("実行するファイルを指定してください．");
                return;
            }
            String[] targetDevices = HidBoot.Enumerate();
            if (targetDevices.Length == 0)
            {
                MessageBox.Show("HIDBootデバイスが見つかりませんでした．接続を確認してください．\nユーザーアプリケーションを実行中の場合はリセットボタンを押してください．");
                return;
            }
            
            HidBoot hidBoot = new HidBoot(targetDevices[0]);
            using (hidBoot)
            {
                this.Enabled = false;
                this.Text = "転送中...";

                FileStream fs = new FileStream(this.binaryText.Text, FileMode.Open);
                SparseImage spm = HexLoader.LoadIntel(fs);
                fs.Close();
                byte[] prog = spm.ToBlockImage();
                hidBoot.WriteApplication(prog, spm.MinimumAddress, spm.MaximumAddress);
                hidBoot.RunApplication();

                this.Enabled = true;
                this.Text = "HIDBoot";

            }
        }

        private void browseButton_Click(object sender, EventArgs e)
        {
            if (this.openTarget.ShowDialog() == DialogResult.OK)
            {
                this.binaryText.Text = this.openTarget.FileName;
            }
        }
    }
}